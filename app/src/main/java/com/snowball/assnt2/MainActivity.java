package com.snowball.assnt2;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;


//MainActivity starts the project with its welcomepage and login pages..
public class MainActivity extends AppCompatActivity
{

    final static int WELCOMEPAGE = 0, LOGINPAGE = 1;
    static RequestQueue queue;
    ViewSwitcher vs;
    int STATE = 0;

// When the app is created...
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vs = (ViewSwitcher) findViewById(R.id.ma_root_vs);
        STATE = WELCOMEPAGE;

        Res.BuildAddress();
        Res.start(getFilesDir());
        DefaultHttpClient httpclient = new DefaultHttpClient();

        //cokies are stored using this..
        BasicCookieStore cookieStore = new BasicCookieStore();
        httpclient.setCookieStore(cookieStore);

        HttpStack httpStack = new HttpClientStack(httpclient);
        queue = Volley.newRequestQueue(this, httpStack);
    }


    //handling back pressed events..
    @Override
    public void onBackPressed()
    {
        if (STATE == WELCOMEPAGE)
        {
            super.onBackPressed();
        } else if (STATE == LOGINPAGE)
        {
            vs.setDisplayedChild(0);
            STATE = WELCOMEPAGE;
        }
    }


    //swiches to login page view..
    public void switchToLogin(View view)
    {
        vs.setDisplayedChild(1);
        STATE = LOGINPAGE;
    }


    //used to change server host addresses..
    public void changeAddress(View view)
    {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.edit_address);
        dialog.setTitle("Enter address");
        dialog.show();
        final EditText et = (EditText) dialog.findViewById(R.id.ea_et);
        et.setText(Res.address());
        ((Button) dialog.findViewById(R.id.ea_button)).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Res.address = et.getText().toString();
                dialog.dismiss();
            }
        });
    }


    //sends login requests to the server
    //the url includes the username and password from the edit texts in the app..
    //also time of login is sent...this will be used to store cookies which expire after a day..
    public void sendLoginRequest(View view)
    {
        final Context context = this;

        Res.UID = ((EditText) findViewById(R.id.ma_1_username_et)).getText().toString();
        Res.password = ((EditText) findViewById(R.id.ma_1_password_et)).getText().toString();
        final String url = Res.address() + "/project/login.jsp?ID=" + Res.UID + "&password=" + Res.password+"&unused="+System.currentTimeMillis();
//        Res.UID = "Rashi";
//        Res.password = "rndPass";
//        final String url = Res.address() + "/project/login.jsp?ID=Rashi&password=rndPass";
        Log.d("ASSN2", url);
        final String uid = ((EditText) findViewById(R.id.ma_1_username_et)).getText().toString();

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String stringResponse)
            {

            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getApplicationContext(), "Error " + error.toString() + "\n" + error.getMessage(), Toast.LENGTH_LONG).show();
                error.printStackTrace();
                Log.d("ASSN2", "ERROR\n" + error.toString());
            }
        })
        {
            //parses the response string...
            //also checks if the user is admin and switches between other activities according to the requirement
            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response)
            {

                String login = response.headers.get("LOGIN");
                if (login.equals("true"))
                {
                    if(Res.UID.equals("admin"))
                    {
                        Intent i = new Intent(context, MainActivity3.class);
                        startActivity(i);
                        return super.parseNetworkResponse(response);
                    }
                    Res.cluster = response.headers.get("CLUSTER");
                    Res.name = response.headers.get("NAME");



                    Res.PD = response.headers.get("PENDING");
                    Res.HD = response.headers.get("HISTORY");
                    Res.ND = response.headers.get("NOTIFICATION");

                    Res.deletefolder(Res.historyFolder);
                    Res.deletefolder(Res.notifFolder);
                    Res.deletefolder(Res.pendingFolder);

                    Res.historyFolder.mkdirs();
                    Res.pendingFolder.mkdirs();
                    Res.notifFolder.mkdirs();

                    Log.d("ASSN2", response.headers.get("PENDING"));
                    Log.d("ASSN2", response.headers.get("HISTORY"));
                    Log.d("ASSN2", response.headers.get("NOTIFICATION"));
                    String[] har = response.headers.get("HISTORY").replace(";", " ").replace("#", "").split(" ");
                    String[] nar = response.headers.get("NOTIFICATION").replace(";", " ").replace("#", "").split(" ");
                    String[] par = response.headers.get("PENDING").replace(";", " ").replace("#", "").split(" ");
                    for (int j = 0; j < har.length; j++)
                    {
                        if (har[j].isEmpty() || har[j].equals(""))
                        {
                            continue;
                        }
                        String add = Res.address() + "/project/serverfiles/forms/" + har[j] + ".form";
                        File file = new File(Res.historyFolder, har[j] + ".form");
                        Log.d("assn2p", add + "\n" + file);
                        new filedownloader(add, file);
                    }
                    for (int j = 0; j < nar.length; j++)
                    {
                        if (nar[j].isEmpty() || nar[j].equals(""))
                        {
                            continue;
                        }
                        String add = Res.address() + "/project/serverfiles/forms/" + nar[j] + ".form";
                        File file = new File(Res.notifFolder, nar[j] + ".form");
                        Log.d("assn2p", add + "\n" + file);
                        new filedownloader(add, file);
                    }
                    for (int j = 0; j < par.length; j++)
                    {
                        if (par[j].isEmpty() || par[j].equals(""))
                        {
                            continue;
                        }
                        String add = Res.address() + "/project/serverfiles/forms/" + par[j] + ".form";
                        File file = new File(Res.pendingFolder, par[j] + ".form");
                        Log.d("assn2p", add + "\n" + file);
                        new filedownloader(add, file);
                    }


                    Intent i = new Intent(context, MainActivity2.class);
                    startActivity(i);

//          This stops the thread for a little bit of time when the user has logged in
                    AsyncTask<Void, Void, Void> async = new AsyncTask<Void, Void, Void>()
                    {
                        //          This stops the thread for a little bit of time...
                        @Override
                        protected Void doInBackground(Void... params)
                        {
                            try
                            {
                                Thread.sleep(2000);
                            } catch (Exception e)
                            {
                            }
                            return null;
                        }

                        //this resets the edit texts of username and password to empty string...
                        @Override
                        protected void onPostExecute(Void result)
                        {
                            try
                            {
                                ((EditText) findViewById(R.id.ma_1_password_et)).setText("");
                                ((EditText) findViewById(R.id.ma_1_username_et)).setText("");
                            } catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                    };
                    async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                } else
                //handles unauthorised users and wrong password errors...
                {
                    if (response.headers.get("REASONCODE").equals("1"))
                    {
                        //wrong password
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                Toast.makeText(context, response.headers.get("REASON"), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    if (response.headers.get("REASONCODE").equals("0"))
                    {
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                Toast.makeText(context, response.headers.get("REASON"), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }
                return super.parseNetworkResponse(response);
            }
        };
        queue.add(stringRequest);
    }
}