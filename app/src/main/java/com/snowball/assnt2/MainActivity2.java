package com.snowball.assnt2;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.net.URI;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

public class MainActivity2 extends AppCompatActivity {

    private static int ext_state = 0;
    private static int form_id;//for adding comment to a particular form
    private static int notifFrom = 0;
    private static String f_toID, f_title, f_body, f_cluster;
    private final int EXT_PENDING = 0, EXT_NEWFORM = 1, EXT_NOTIFICATION = 2, EXT_VIEWFORM = 3;
    ArrayList<Notification> hl, pl, nl;
    Notification present = null;
    boolean refreshbool = true;
    private ViewFlipper vf_ext;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;



    //to count the length of the string..
    //just another helper function to be ignored by all(humans)...
    //i.e its a servant..
    static int countof(String findStr, String str) {
        int lastIndex = 0;
        int count = 0;

        while (lastIndex != -1) {

            lastIndex = str.indexOf(findStr, lastIndex);

            if (lastIndex != -1) {
                count++;
                lastIndex += findStr.length();
            }
        }
        return count;
    }

    //after creating this activity for the first time..do
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
  //the floatingactionbutton refreshes everything...
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshall();
            }
        });

        vf_ext = (ViewFlipper) findViewById(R.id.cm2_vf);
        ((Button) findViewById(R.id.cm2_btn0_pending)).setEnabled(false);


        //  This stops the thread for a little bit of time when the user has logged in
        AsyncTask<Void, Void, Void> async = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Thread.sleep(2000);
                } catch (Exception e) {
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                refreshLists();
                refreshViews();
                try {
                    Log.d("assn2oc", "" + Res.pendingFolder.listFiles().length);
                    Log.d("assn2oc", "" + Res.notifFolder.listFiles().length);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        final Context context = this;
    //The options to be selected in the spinners used are set here..
        //Links the addressee list of the complaint with the type of complaint...
        ((Spinner) findViewById(R.id.cm2_vf1_spinner_newCmpType)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    ((Spinner) findViewById(R.id.cm2_vf1_spinner_Addressee)).setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, context.getResources().getStringArray(R.array.Indi)));
                }
                if (position == 1) {
                    ((Spinner) findViewById(R.id.cm2_vf1_spinner_Addressee)).setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, context.getResources().getStringArray(R.array.Hostel)));
                }
                if (position == 2) {
                    ((Spinner) findViewById(R.id.cm2_vf1_spinner_Addressee)).setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, context.getResources().getStringArray(R.array.Insti)));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_cp:
                final Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.edit_address);
                dialog.setTitle("New Password...");
                dialog.show();
                final EditText et = (EditText) dialog.findViewById(R.id.ea_et);
                et.setText(Res.password);
                et.setHint("Enter new Password...");
                

                //handles requests to set new passwords when the corresponding button is pressed..
                ((Button) dialog.findViewById(R.id.ea_button)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String newpass = et.getText().toString();
                        if (newpass.isEmpty()) {
                            Toast.makeText(getApplication(), "Empty Password not allowed", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        String url = Res.address() + "/project/changePassword.jsp?ID=" + Res.UID + "&newpassword=" + newpass;
                        Log.d("ASSN2NOTIF", url);
                        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String stringResponse) {
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(), "Error " + error.toString() + "\n" + error.getMessage(), Toast.LENGTH_LONG).show();
                                error.printStackTrace();
                                Log.d("ASSN2", "ERROR\n" + error.toString());
                            }
                        }) {
                            @Override
                            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                                try {
                                    if (response.headers.get("DONE").equals("true")) {
                                        dialog.dismiss();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return super.parseNetworkResponse(response);
                            }
                        };
                        MainActivity.queue.add(stringRequest);
                    }
                });
                break;
            case R.id.menu_lo:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }


    //handles back pressed events

    public void onBackPressed() {
        switch (ext_state) {
            case EXT_PENDING:
                super.onBackPressed();
            case EXT_NEWFORM:
                super.onBackPressed();
                break;
            case EXT_NOTIFICATION:
                super.onBackPressed();
                break;
            case EXT_VIEWFORM:
                ext_state = notifFrom;
                break;
        }
        vf_ext.setDisplayedChild(ext_state);

    }


    //gets all the notifications and pending requests from the server...
    ArrayList<Notification> getForms(File[] far) {
        ArrayList<Notification> notArr = new ArrayList<Notification>();
        try {
            for (int i = 0; i < far.length; i++) {
                FileInputStream fileIn = new FileInputStream(far[i]);
                ObjectInputStream ins = new ObjectInputStream(fileIn);
                HashMap<String, String> map = (HashMap<String, String>) ins.readObject();
                ins.close();
                fileIn.close();
                notArr.add(new Notification(map));
                //sort the notifications according to time................
                Collections.sort(notArr);
            }
        } catch (Exception e) {
        }
        return notArr;
    }



    //fills the layouts of notification with notifications..
    void fill(LinearLayout ll, ArrayList<Notification> notArr, int initchild, final int prop) {
        //initChild the number from which the ll should be filled
        //prop  0-pending, 1-notification
        int numChild = ll.getChildCount();
        for (int i = 0; i < (numChild - initchild); i++) {
            ll.removeViewAt(initchild);
        }
        try {
            //fill all the notifications in the notification view's layout..
            for (int i = notArr.size() - 1; i >= 0; i--) {
                int upvote = countof("#", notArr.get(i).upvote);
                int downvote = countof("#", notArr.get(i).downvote);
                String time = (new SimpleDateFormat("dd MMM yyyy, hh:mm")).format(new Date(notArr.get(i).time));
                String text = "";
                switch (prop) {
                    case EXT_PENDING:
                        if (Res.UID.equals(notArr.get(i).to)) {
                            text = "<big><b>" + notArr.get(i).from + "</b>" + "   " + time + "</big><br>" + "<b>" + notArr.get(i).title + "</b><br>" + "Upvote:" + upvote + "  Downvote:" + downvote;
                        } else {
                            text = "<big><b>" + notArr.get(i).to + "</b>" + "    " + time + "</big><br>" + "<b>" + notArr.get(i).title + "</b><br>" + "Upvote:" + upvote + "  Downvote:" + downvote;
                        }
                        break;
                    case EXT_NOTIFICATION:
                        text = "<big>From " + notArr.get(i).from + "   " + "To " + notArr.get(i).to + "   " + time + "</big><br>" + "<b>" + notArr.get(i).title + "</b><br>" + "Upvote:" + upvote + "  Downvote:" + downvote;
                        break;
                }
//                <p>From /To Time</p>
//
//                <p><b>Title </b></p>
//
//                <p>Upvote:34 Downvote:34</p>
                View element = LayoutInflater.from(this).inflate(R.layout.e, ll, false);
                TextView notif_tt = (TextView) element.findViewById(R.id.eg_tv);
                notif_tt.setText(Html.fromHtml(text));
                ll.addView(element);
                final Notification n = notArr.get(i);
                element.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        notifFrom = prop;
                        present = n;
                        String usrstr = "#" + Res.UID + ";";
                        final Button up = ((Button) findViewById(R.id.cm2_vf3_button_upvote)), down = ((Button) findViewById(R.id.cm2_vf3_button_downvote)), comment = ((Button) findViewById(R.id.cm2_vf3_button_comment));

                        setFormView();
                        //handles upvote given by the user..
                        up.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String formID = present.from + "_" + present.time;
                                String url = Res.address() + "/project/updownvotecomment.jsp?userID=" + Res.UID + "&password=" + Res.password + "&formID=" + formID + "&updownvote=1";
                                Log.d("ASSN2NOTIF", url);
                                final StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String stringResponse) {
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getApplicationContext(), "Error " + error.toString() + "\n" + error.getMessage(), Toast.LENGTH_LONG).show();
                                        error.printStackTrace();
                                        Log.d("ASSN2", "ERROR\n" + error.toString());
                                    }
                                }) {
                                    @Override
                                    protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                                        try {
                                            if (response.headers.get("DONE").equals("true")) {
                                                n.upvote = n.upvote + "#" + Res.UID + ";";
                                                present = n;
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        setFormView();
                                                    }
                                                });
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        return super.parseNetworkResponse(response);
                                    }
                                };
                                MainActivity.queue.add(stringRequest);
                            }
                        });

                        //handles downvote given by the user..
                        down.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String formID = present.from + "_" + present.time;
                                String url = Res.address() + "/project/updownvotecomment.jsp?userID=" + Res.UID + "&password=" + Res.password + "&formID=" + formID + "&updownvote=-1";
                                Log.d("ASSN2NOTIF", url);
                                final StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String stringResponse) {
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getApplicationContext(), "Error " + error.toString() + "\n" + error.getMessage(), Toast.LENGTH_LONG).show();
                                        error.printStackTrace();
                                        Log.d("ASSN2", "ERROR\n" + error.toString());
                                    }
                                }) {
                                    @Override
                                    protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                                        try {
                                            if (response.headers.get("DONE").equals("true")) {
                                                n.downvote = n.downvote + "#" + Res.UID + ";";
                                                present = n;
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        setFormView();
                                                    }
                                                });
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        return super.parseNetworkResponse(response);
                                    }
                                };
                                MainActivity.queue.add(stringRequest);
                            }
                        });

                        //handles comments given by the user..
                        //i.e comments are sent to the server...
                        comment.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String formID = present.from + "_" + present.time;
                                final String comment = ((EditText) findViewById(R.id.cm2_vf3_et_comment)).getText().toString().replace("#", "").replace(";", "");
                                String urlStr = Res.address() + "/project/updownvotecomment.jsp?userID=" + Res.UID + "&password=" + Res.password + "&formID=" + formID + "&comment=" + comment + "";
                                URL url = null;
                                try {
                                    url = new URL(urlStr);
                                    URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
                                    url = uri.toURL();
                                } catch (Exception e) {

                                }
                                Log.d("ASSN2NOTIF", url.toString());
                                final StringRequest stringRequest = new StringRequest(Request.Method.POST, ((url == null) ? urlStr : url.toString()), new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String stringResponse) {
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getApplicationContext(), "Error " + error.toString() + "\n" + error.getMessage(), Toast.LENGTH_LONG).show();
                                        error.printStackTrace();
                                        Log.d("ASSN2", "ERROR\n" + error.toString());
                                    }
                                }) {
                                    @Override
                                    protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                                        try {
                                            if (response.headers.get("DONE").equals("true")) {
                                                n.comments = (n.comments + "#" + Res.UID + ";" + comment);
                                                present = n;
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        setFormView();
                                                    }
                                                });
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        return super.parseNetworkResponse(response);
                                    }
                                };
                                MainActivity.queue.add(stringRequest);
                            }
                        });

                        //design text
                        //assign on clicks to upvotes/downvotes/comment
                        //if successful
                        ext_state = EXT_VIEWFORM;
                        vf_ext.setDisplayedChild(ext_state);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //this hnadles switches between the three views of the view flipper..
    //the user can see his pending requests in pending view...
    //notifications in nitifications view..
    //and to post a new complaint he can switch to the new complaint view...
    public void bbAssignmentClicked(View v) {
        ((Button) findViewById(R.id.cm2_btn0_pending)).setEnabled(true);
        ((Button) findViewById(R.id.cm2_btn1_newCmplnt)).setEnabled(true);
        ((Button) findViewById(R.id.cm2_btn2_Notifs)).setEnabled(true);

        switch (v.getId()) {
            case R.id.cm2_btn0_pending:
                ext_state = EXT_PENDING;
                vf_ext.setDisplayedChild(ext_state);
                ((Button) findViewById(R.id.cm2_btn0_pending)).setEnabled(false);
                break;

            case R.id.cm2_btn1_newCmplnt:
                ext_state = EXT_NEWFORM;
                vf_ext.setDisplayedChild(ext_state);
                ((Button) findViewById(R.id.cm2_btn1_newCmplnt)).setEnabled(false);
                break;

            case R.id.cm2_btn2_Notifs:
                ext_state = EXT_NOTIFICATION;
                vf_ext.setDisplayedChild(ext_state);
                ((Button) findViewById(R.id.cm2_btn2_Notifs)).setEnabled(false);
                break;
        }
    }




    //refreshes tho locally stored files..
    //the files are refreshed regularly by default..
    void refreshfiles() {
        final String url = Res.address() + "/project/login.jsp?ID=" + Res.UID + "&password=" + Res.password;
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String stringResponse) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String login = response.headers.get("LOGIN");
                if (login.equals("true")) {
                    Res.deletefolder(Res.pendingFolder);
                    Res.pendingFolder.mkdirs();
                    Log.d("ASSN2", response.headers.get("PENDING"));
                    String[] par = response.headers.get("PENDING").replace(";", " ").replace("#", "").split(" ");
                    for (int j = 0; j < par.length; j++) {
                        if (par[j].isEmpty() || par[j].equals("")) {
                            continue;
                        }
                        String add = Res.address() + "/project/serverfiles/forms/" + par[j] + ".form";
                        File file = new File(Res.pendingFolder, par[j] + ".form");
                        Log.d("assn2p", add + "\n" + file);
                        new filedownloader(add, file);
                    }
                    Res.historyFolder.mkdirs();
                    Res.deletefolder(Res.historyFolder);
                    Log.d("ASSN2", response.headers.get("HISTORY"));
                    String[] har = response.headers.get("HISTORY").replace(";", " ").replace("#", "").split(" ");
                    for (int j = 0; j < har.length; j++) {
                        if (har[j].isEmpty() || har[j].equals("")) {
                            continue;
                        }
                        String add = Res.address() + "/project/serverfiles/forms/" + har[j] + ".form";
                        File file = new File(Res.historyFolder, har[j] + ".form");
                        Log.d("assn2p", add + "\n" + file);
                        new filedownloader(add, file);
                    }
                    Res.deletefolder(Res.notifFolder);
                    Res.notifFolder.mkdirs();
                    Log.d("ASSN2", response.headers.get("NOTIFICATION"));
                    String[] nar = response.headers.get("NOTIFICATION").replace(";", " ").replace("#", "").split(" ");
                    for (int j = 0; j < nar.length; j++) {
                        if (nar[j].isEmpty() || nar[j].equals("")) {
                            continue;
                        }
                        String add = Res.address() + "/project/serverfiles/forms/" + nar[j] + ".form";
                        File file = new File(Res.notifFolder, nar[j] + ".form");
                        Log.d("assn2p", add + "\n" + file);
                        new filedownloader(add, file);
                    }
//                    }
                }
                return super.parseNetworkResponse(response);
            }
        };
        MainActivity.queue.add(stringRequest);
    }

    //this refreshes tthe pending requests and notifications....
    void refreshLists() {
        pl = getForms(Res.pendingFolder.listFiles());
        nl = getForms(Res.notifFolder.listFiles());
    }


    //refreshes all the views..
    void refreshViews() {
        LinearLayout pendingll = (LinearLayout) findViewById(R.id.cm2_vf0_ll_Pending);
        LinearLayout notifll = (LinearLayout) findViewById(R.id.cm2_vf2_ll_notif);
        fill(pendingll, pl, 1, EXT_PENDING);
        fill(notifll, nl, 1, EXT_NOTIFICATION);
    }


    //sets the complaints that are to be read.
    //i.e when a notification or a pending complaint is pressed, the complaint is displayed by this function..
    void setFormView() {
        ((EditText) findViewById(R.id.cm2_vf3_et_comment)).setText("");
        StringBuilder sb = new StringBuilder();
        String line1 = "<small>From : <b>" + present.from + "  </b>To : <b>" + present.to + "</b></small>";
        String line2 = "<br><small><u>" + (new SimpleDateFormat("dd MMM yyyy, hh:mm:ss")).format(new Date(present.time)) + "</u></small><br><br>";
        String line3 = "<b>Ups :</b> <font color='pink'>" + countof("#", present.upvote) + "</font> /<b>  Downs : </b><font color='pink'>" + countof("#", present.downvote) + "</font>, ";
        if (present.status.equals("0")) {
            line3 += "  <b><font color='red'>PENDING</font></b><br><br>";
        } else {
            line3 += "  <b><font color='green'>RESOLVED</font></b><br><br>";
        }
        String line4 = "<big><b>" + present.title + "</b></big><br>";
        String line5 = present.body + "<br><br><br>" +
                "";
        sb.append(line4);
        sb.append(line1);
        sb.append(line2);
        sb.append(line5);
        sb.append(line3);
        try {
            String[] coms = present.comments.replace(";", " : ").split("#");
            for (int i = 0; i < coms.length; i++) {
                if (coms[i].isEmpty()) {
                    continue;
                }
                String line = "<b>" + coms[i].substring(0, coms[i].indexOf(": ")) + "</b>";
                line += coms[i].substring(coms[i].indexOf(": ")) + "<br>";
                sb.append(line);
            }

            sb.append("<br>");
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("ASSSNSS", present.comments);
        }

        //sets the text viewvs in the layout..
        ((TextView) findViewById(R.id.cm2_vf3_tv_viewNotifDescr)).setText(Html.fromHtml(sb.toString()));
        String usrstr = "#" + Res.UID + ";";
        final Button up = ((Button) findViewById(R.id.cm2_vf3_button_upvote)), down = ((Button) findViewById(R.id.cm2_vf3_button_downvote)), comment = ((Button) findViewById(R.id.cm2_vf3_button_comment));

        if (present.downvote.contains(usrstr) || present.upvote.contains(usrstr)) {
            up.setEnabled(false);
            down.setEnabled(false);
        } else {
            up.setEnabled(true);
            down.setEnabled(true);
        }
        if (present.to.equals(Res.UID)) {
            if (present.status.equals("0")) {
                (findViewById(R.id.cm2_vf3_button_resolve)).setVisibility(View.VISIBLE);
            } else {
                (findViewById(R.id.cm2_vf3_button_resolve)).setVisibility(View.GONE);
            }
            final String presentformID = present.from + "_" + present.time;


//            handles the resolve button...
            (findViewById(R.id.cm2_vf3_button_resolve)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String url = Res.address() + "/project/changeStatus.jsp?formID=" + presentformID + "&status=1";
                    Log.d("ASSN2", url);
                    final StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String stringResponse) {

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), "Error " + error.toString() + "\n" + error.getMessage(), Toast.LENGTH_LONG).show();
                            error.printStackTrace();
                            Log.d("ASSN2", "ERROR\n" + error.toString());
                        }
                    }) {
                        @Override
                        protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                            try {
                                if (response.headers.get("DONE").equals("true")) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            ext_state = notifFrom;
                                            vf_ext.setDisplayedChild(ext_state);
                                            refreshall();
                                        }
                                    });
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return super.parseNetworkResponse(response);
                        }
                    };
                    MainActivity.queue.add(stringRequest);
                }
            });
        } else {
            //after the complaint is resolved, the resolve button vanishes..
            (findViewById(R.id.cm2_vf3_button_resolve)).setVisibility(View.GONE);
        }

    }

//    refreshes everything
    void refreshall() {
        if (!refreshbool) {
            return;
        }
        AsyncTask<Void, Void, Void> async = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                refreshbool = false;
                refreshfiles();
                try {
                    Thread.sleep(2000);
                } catch (Exception e) {
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                refreshbool = true;
                refreshLists();
                refreshViews();
                try {
                    Log.d("assn2oc", "" + Res.pendingFolder.listFiles().length);
                    Log.d("assn2oc", "" + Res.notifFolder.listFiles().length);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    //handles comments..
    //new complaints are posted by invoking this function..
    public void PostNewComment(View view) {
        Spinner stype = (Spinner) findViewById(R.id.cm2_vf1_spinner_newCmpType);
        Spinner sadd = (Spinner) findViewById(R.id.cm2_vf1_spinner_Addressee);

        f_toID = "toID";
        switch (stype.getSelectedItemPosition()) {
            case 0:
                f_toID = getResources().getStringArray(R.array.Indi)[sadd.getSelectedItemPosition()];
                f_cluster = "INDIVIDUAL";
                break;
            case 1:
                f_toID = Res.cluster + "_" + getResources().getStringArray(R.array.Hostel)[sadd.getSelectedItemPosition()];
                f_cluster = "HOSTEL";
                break;
            case 2:
                f_toID = getResources().getStringArray(R.array.Insti)[sadd.getSelectedItemPosition()];
                f_cluster = "ELSE";
                break;
        }
        f_body = ((EditText) findViewById(R.id.cm2_vf1_et_Description)).getText().toString();
        f_title = ((EditText) findViewById(R.id.cm2_vf1_et_newCompTitle)).getText().toString();
        if (f_title.isEmpty()) {
            Toast.makeText(getApplicationContext(), "missing title", Toast.LENGTH_SHORT).show();
            return;
        }
        if (f_body.isEmpty()) {
            Toast.makeText(getApplicationContext(), "missing description", Toast.LENGTH_SHORT).show();
            return;
        }


        final String urlStr = Res.address() + "/project/addForm.jsp?toID=" + f_toID + "&userID=" + Res.UID + "&cluster=" + f_cluster + "&title=" + f_title + "&body=" + f_body;
        try {
            URL url = new URL(urlStr);
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            url = uri.toURL();
            Log.d("ASSN@FORM", url.toString());
            final StringRequest stringRequest = new StringRequest(Request.Method.POST, url.toString(), new Response.Listener<String>() {
                @Override
                public void onResponse(String stringResponse) {

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "Error " + error.toString() + "\n" + error.getMessage(), Toast.LENGTH_LONG).show();
                    error.printStackTrace();
                    Log.d("ASSN2", "ERROR\n" + error.toString());
                }
            }) {

                //after the new complaint is filed..it resets the description and title edit texts to empty...
                @Override
                protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                    try {
                        if (response.headers.get("DONE").equals("true")) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Complaint registered", Toast.LENGTH_LONG).show();
                                    ((EditText) findViewById(R.id.cm2_vf1_et_Description)).setText("");
                                    ((EditText) findViewById(R.id.cm2_vf1_et_newCompTitle)).setText("");
                                    refreshall();
                                }
                            });
                        }
                    } catch (Exception e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Unable to add complaint", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    return super.parseNetworkResponse(response);
                }
            };
            MainActivity.queue.add(stringRequest);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

