package com.snowball.assnt2;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
public class Res
{
	static String url = "10.192.58.231";
	static int port = 7080;
	static String address = "http://"+Res.url +":"+ Res.port;
	static void BuildAddress()
	{
		address = "http://"+Res.url +":"+ Res.port;
	}
	static String address()
	{
		return address;
	}

	static File root, historyFolder,pendingFolder, notifFolder;
	static String UID,password,ND,PD,HD,cluster,name;
	static void start(File root)
	{
		Res.root = root;
		historyFolder = new File(root.getAbsolutePath()+System.getProperty("file.separator")+"History");
		pendingFolder = new File(root.getAbsolutePath()+System.getProperty("file.separator")+"Pending");
		notifFolder = new File(root.getAbsolutePath()+System.getProperty("file.separator")+"Notif");
		historyFolder.mkdirs();
		pendingFolder.mkdirs();
		notifFolder.mkdirs();
	}

	public static void deletefolder(File folder)
	{
		if(!folder.exists())
		{
			return;
		}
		File[] roots = folder.listFiles();
		try
		{
			for (File x : roots)
			{
				if (x.isDirectory())
				{
					deletefolder(x);
					x.delete();
				}
				if (x.isFile())
				{
					x.delete();
				}
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		folder.delete();
	}
}

class filedownloader
{
	private File filetosaveon;
	private String completefileurl;
	public boolean done = false;

	private AsyncTask<Void, Void, Void> async = new AsyncTask<Void, Void, Void>()
	{
		@Override
		protected void onPreExecute()
		{
		}

		@Override
		protected Void doInBackground(Void... urls)
		{

			// params comes from the execute() call: params[0] is the url.
			try
			{
				URL url_conn = new URL(completefileurl);
				InputStream in = new BufferedInputStream(url_conn.openStream());
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				byte[] buf = new byte[1024];
				int n = 0;
				while (-1 != (n = in.read(buf)))
				{
					out.write(buf, 0, n);
				}
				out.close();
				in.close();
				byte[] response = out.toByteArray();

				FileOutputStream fos = new FileOutputStream(filetosaveon);
				fos.write(response);
				fos.close();
				//End download code
			} catch (Exception e)
			{
				e.printStackTrace();
				return null;
			}
			return null;
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(Void result)
		{
			done = true;
			Log.d("done",""+filetosaveon.exists());
		}
	};

	filedownloader(String completefileurl, File filetosaveon)
	{
		completefileurl = completefileurl + "?unused=" + System.currentTimeMillis();
		this.completefileurl = completefileurl;
		this.filetosaveon = filetosaveon;
		async.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}
}