package com.snowball.assnt2;

import java.util.HashMap;

/**
 * Created by Mukesh on 28/03/16.
 */
public class Notification implements Comparable<Notification>
{
    public String title, body, status, from, to, cluster, text;
    public long time;
    public String upvote, downvote;
    public String comments;

    public Notification(HashMap<String, String> map)
    {
        this.title = map.get("TITLE");
        this.body = map.get("BODY");
        this.status = map.get("STATUS");
        this.from = map.get("FROMID");
        this.to = map.get("TOID");
        this.time = Long.parseLong(map.get("TIME"));
        this.comments = map.get("COMMENT");
        this.upvote = map.get("UPVOTE");
        this.downvote = map.get("DOWNVOTE");
        this.cluster = map.get("CLUSTER");
        this.text = "";
    }


    @Override
    public int compareTo(Notification another)
    {

        return (this.time > another.time) ? 1 : -1;
    }
}
