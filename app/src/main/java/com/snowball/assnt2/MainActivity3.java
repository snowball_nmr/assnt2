package com.snowball.assnt2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

public class MainActivity3 extends AppCompatActivity {

    private ViewFlipper vf_ext_cm3;
    private final int VF_State_Add =0, VF_State_Delete=1;
    private int VFState; // Current state of view flipper

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        vf_ext_cm3 = (ViewFlipper) findViewById(R.id.cm3_vf);
        ((Button) findViewById(R.id.cm3_ll_btn_AddUser)).setEnabled(false);
        VFState = VF_State_Add; //

        final Button AddUserAdd = (Button) findViewById(R.id.cm3_vf0_btn_AddUserAdd), DeleteUserDel = (Button)(findViewById(R.id.cm3_vf1_ll_btn_DeleteUser));
        final Button CancelAddUsr = (Button)findViewById(R.id.cm3_vf0_btn_AddUserCancel), CancelDelUSr = (Button)findViewById(R.id.cm3_vf1_ll_btn_DeleteCancel);


        //this function adds the user to the database by taking the required fields from the app's UI

        AddUserAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Username = ((EditText) (findViewById(R.id.cm3_vf0_et_AddUserUsrName))).getText().toString();
                String randomPassword = ((EditText) (findViewById(R.id.cm3_vf0_et_AddUserPWD))).getText().toString();
                String userID = ((EditText) (findViewById(R.id.cm3_vf0_et_AddUserUID))).getText().toString();
                String cluster = ((EditText) (findViewById(R.id.cm3_vf0_et_AddUserHostel))).getText().toString();

//                String
//                final String comment = ((EditText) findViewById(R.id.cm2_vf3_et_comment)).getText().toString().replace(" ", "%20");
                String url = Res.address() + "/project/addUser.jsp?userID=" + userID + "&randomPassword=" + randomPassword + "&Username=" + Username + "&cluster=" + cluster + "";
               // Log.d("ASSN2NOTIF", url);
                final StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String stringResponse) {
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Error " + error.toString() + "\n" + error.getMessage(), Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                        Log.d("ASSN2", "ERROR\n" + error.toString());
                    }
                });
                MainActivity.queue.add(stringRequest);
            }
        });



        //deletes the user from database if he is present..
        DeleteUserDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userID = ((EditText) (findViewById(R.id.cm3_vf1_et_DeleteUserUID))).getText().toString();
//                String cluster = ((EditText) (findViewById(R.id.cm3_vf0_et_AddUserHostel))).getText().toString();

//                String
//                final String comment = ((EditText) findViewById(R.id.cm2_vf3_et_comment)).getText().toString().replace(" ", "%20");
                String url = Res.address() + "/project/deleteUser.jsp?userID=" + userID + "";
                // Log.d("ASSN2NOTIF", url);
                final StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String stringResponse) {
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Error " + error.toString() + "\n" + error.getMessage(), Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                        Log.d("ASSN2", "ERROR\n" + error.toString());
                    }
                });
                MainActivity.queue.add(stringRequest);
            }
        });


        //Resets the edit text views in the Adduser pane..
        CancelAddUsr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((EditText) (findViewById(R.id.cm3_vf0_et_AddUserUsrName))).setText("");
                ((EditText) (findViewById(R.id.cm3_vf0_et_AddUserPWD))).setText("");
                ((EditText) (findViewById(R.id.cm3_vf0_et_AddUserUID))).setText("");
                ((EditText) (findViewById(R.id.cm3_vf0_et_AddUserHostel))).setText("");
            }

            });

        //resets all the edit texts in the DeleteUSer pane..
        CancelDelUSr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((EditText) (findViewById(R.id.cm3_vf0_et_AddUserUsrName))).setText("");
//                ((EditText) (findViewById(R.id.cm3_vf0_et_AddUserPWD))).setText("");
                ((EditText) (findViewById(R.id.cm3_vf1_et_DeleteUserUID))).setText("");
//                ((EditText) (findViewById(R.id.cm3_vf0_et_AddUserHostel))).setText("");
            }

        });
    }


//    Handles switches between AddUser and DeleteUser panes..
    public void viewChange (View v) {
        ((Button) findViewById(R.id.cm3_ll_btn_AddUser)).setEnabled(true);
        ((Button) findViewById(R.id.cm3_ll_btn_DeleteUser)).setEnabled(true);
        //((Button) findViewById(R.id.cm2_btn2_Notifs)).setEnabled(true);

        switch (v.getId()) {
            case R.id.cm3_ll_btn_AddUser:
                VFState = VF_State_Add;
                vf_ext_cm3.setDisplayedChild(VFState);
                ((Button) findViewById(R.id.cm3_ll_btn_AddUser)).setEnabled(false);
                break;

            case R.id.cm3_ll_btn_DeleteUser:
                VFState = VF_State_Delete;
                vf_ext_cm3.setDisplayedChild(VFState);
                ((Button) findViewById(R.id.cm3_ll_btn_DeleteUser)).setEnabled(false);
                break;

        }
    }


    //handles on backpressed eevnts..
        public void onBackPressed() {
            switch (VFState) {
                case VF_State_Add:
                    super.onBackPressed();

                case VF_State_Delete:
                    VFState = VF_State_Add;
                    break;
            }
            vf_ext_cm3.setDisplayedChild(VFState);

        }



    }


